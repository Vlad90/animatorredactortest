﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class deleteButton : MonoBehaviour
{
    private Button thisButton;

    void Awake()
    {
        thisButton = this.GetComponent<Button>();
    }

    void Update()
    {
        thisButton.interactable = (Vertex.selectedVertex != null || EdgeController.selectedEdge != null);
    }
}
