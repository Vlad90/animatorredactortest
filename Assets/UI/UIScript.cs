﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections.Generic;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;
using System;

public class UIScript : MonoBehaviour {
    public GameObject helpPanel; 

	void Update () {
        if (Input.GetKeyUp(KeyCode.A))
        {
            FrameInfo.previousFrame();
        }
        if (Input.GetKeyUp(KeyCode.D))
        {
            FrameInfo.nextFrame();
        }
        if (Input.GetKeyUp(KeyCode.C))
        {
            Vector2 pos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            Vertex.CreateVertex(pos);
        }
        if (Input.GetKeyUp(KeyCode.Delete))
        {
            Delete();
        }
        if (Input.GetKeyUp(KeyCode.Backspace))
        {
            Delete();
        }
        if (Input.GetKeyUp(KeyCode.S))
        {
            Save();
        }
        if (Input.GetKeyUp(KeyCode.L))
        {
            Load();
        }
        if (Input.GetKeyUp(KeyCode.F1))
        {
            Help();
        }
        if (Input.GetKeyUp(KeyCode.Escape) && helpPanel.activeSelf)
        {
            DisableHelp();
        }
    }

    public void Delete()
    {
        if (Vertex.selectedVertex != null)
        {
            Destroy(Vertex.selectedVertex.gameObject);
        }
        if (EdgeController.selectedEdge != null)
        {
            Destroy(EdgeController.selectedEdge.gameObject);
        }
    }

    public void Save()
    {
        BinaryFormatter formatter = new BinaryFormatter();
        List<SerializableFrame> frameList = new List<SerializableFrame>();
        FrameInfo frame = FrameInfo.firstFrame;
        while (frame != null)
        {
            frameList.Add(frame.toSerializableFrame());
            frame = frame.NextFrameInfo;
        }
        using (FileStream fs = new FileStream("animation.anim", FileMode.OpenOrCreate))
        {
            formatter.Serialize(fs, frameList);
        }
    }

    public void Load()
    {
        FrameInfo frame = FrameInfo.firstFrame;
        while (frame != null)
        {
            FrameInfo tmp = frame.NextFrameInfo;
            Destroy(frame.gameObject);
            frame = tmp;
        }
        BinaryFormatter formatter = new BinaryFormatter();
        List<SerializableFrame> frameList;
        using (FileStream fs = new FileStream("animation.anim", FileMode.OpenOrCreate))
        {
            frameList = (List<SerializableFrame>)formatter.Deserialize(fs);
        }
        FrameInfo prevFrame = FrameInfo.deserializeFrame(frameList[0]);
        for (int i = 1; i < frameList.Count; ++i)
        {
            prevFrame = FrameInfo.deserializeFrame(frameList[i], prevFrame);
        }
        FrameInfo.currentFrame = FrameInfo.firstFrame;
    }

    public void Exit()
    {
        Application.Quit();
    }

    public void Help()
    {
        helpPanel.SetActive(true);
    }

    public void DisableHelp()
    {
        helpPanel.SetActive(false);
    }
}
