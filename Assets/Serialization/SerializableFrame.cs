﻿using System;
using System.Collections.Generic;
using System.Text;
using UnityEngine;

[Serializable]
public class Pair
{
    public float x, y;
    public Pair(Vector2 vector)
    {
        x = vector.x;
        y = vector.y;
    }
    public Vector2 toVector()
    {
        return new Vector2(x, y);
    }
}

[Serializable]
public class SerializableFrame
{
    private Pair[] vertexArray;
    private bool[,] isConnected;

    public Pair[] VertexArray
    {
        get
        {
            return vertexArray;
        }
    }

    public bool[,] IsConnected
    {
        get
        {
            return isConnected;
        }
    }

    public SerializableFrame(int vertexNumber)
    {
        vertexArray = new Pair[vertexNumber];
        isConnected = new bool[vertexNumber, vertexNumber];
    }

    public void AddEdge(EdgeController edge)
    {
        isConnected[edge.Vertexes[0].Uid, edge.Vertexes[1].Uid] = true;
    }

    public void AddVertex(Vertex vertex)
    {
        vertexArray[vertex.Uid] = new Pair(vertex.transform.position);
    }
}

