﻿using UnityEngine;
using System.Collections.Generic;
using System;

public class FrameInfo : MonoBehaviour {
    public GameObject edges;
    public GameObject vertexes;
    public static FrameInfo currentFrame;
    public static FrameInfo firstFrame;


    private int vertexUidCounter;
    private int frameNumber;
    private FrameInfo nextFrameInfo, previousFrameInfo;

    public int FrameNumber
    {
        get
        {
            return frameNumber;
        }
    }

    public FrameInfo NextFrameInfo
    {
        get
        {
            return nextFrameInfo;
        }
    }

    public FrameInfo PreviousFrameInfo
    {
        get
        {
            return previousFrameInfo;
        }
    }

    public int VertexUidCounter
    {
        get
        {
            int tmp = vertexUidCounter;
            vertexUidCounter++;
            return tmp;
        }
    }

    public static void nextFrame()
    {
        currentFrame.gameObject.SetActive(false);
        if (currentFrame.NextFrameInfo == null)
        {
            currentFrame.generateNewFrame();
        }
        else
        {
            currentFrame.NextFrameInfo.gameObject.SetActive(true);
        }       
    }

    public static void previousFrame()
    {
        if (currentFrame.PreviousFrameInfo != null)
        {
            currentFrame.gameObject.SetActive(false);
            currentFrame.PreviousFrameInfo.gameObject.SetActive(true);
        }
    }

    public void UpdateUI()
    {
        FrameNumberField.frameNumberField.text = FrameNumber.ToString();
        currentFrame = this;
    }

    private void generateNewFrame()
    {
        FrameInfo tmp = Instantiate(GameInfo.framePrefab).GetComponent<FrameInfo>();
        tmp.frameNumber = FrameNumber + 1;
        tmp.previousFrameInfo = this;
        nextFrameInfo = tmp;
        tmp.UpdateUI();
    }

    void OnEnable()
    {
        UpdateUI();
    }
   
    void Start ()
    {
	}
	
	void Update ()
    {
        
    }

    public SerializableFrame toSerializableFrame()
    {
        SerializableFrame result = new SerializableFrame(vertexUidCounter);
        foreach (Vertex vertex in vertexes.GetComponentsInChildren<Vertex>())
        {
            result.AddVertex(vertex);
        }
        foreach (EdgeController edge in edges.GetComponentsInChildren<EdgeController>())
        {
            result.AddEdge(edge);
        }
        return result;
    }

    public static FrameInfo deserializeFrame(SerializableFrame serializableFrame, FrameInfo previousFrame = null)
    {
        FrameInfo tmp = Instantiate(GameInfo.framePrefab).GetComponent<FrameInfo>();
        if (previousFrame == null)
        {
            firstFrame = tmp;
            tmp.frameNumber = 0;
        }
        else
        {
            tmp.gameObject.SetActive(false);
            tmp.previousFrameInfo = previousFrame;
            previousFrame.nextFrameInfo = tmp;
            tmp.frameNumber = previousFrame.frameNumber + 1;
        }
        List<Vertex> vertexArray = new List<Vertex>(serializableFrame.VertexArray.Length);
        foreach (Pair pair in serializableFrame.VertexArray)
        {
            vertexArray.Add(Vertex.CreateVertex(pair.toVector(), tmp));
        }
        for (int i = 0; i < vertexArray.Count; ++i)
        {
            for (int j = 0; j < vertexArray.Count; ++j)
            {
                if (serializableFrame.IsConnected[i, j])
                {
                    Vertex.ConnectVertexes(vertexArray[i], vertexArray[j]);
                }
            }
        }
        return tmp;
    }
}
