﻿using UnityEngine;
using System.Collections.Generic;
using System;

public class Vertex : MonoBehaviour {
    public static Vertex selectedVertex;

    private bool isSelected = false;
    private int uid;
    private Animator animatorComponent;
    private List<EdgeSide> edges;

    public int Uid
    {
        get
        {
            return uid;
        }
    }

    // Use this for initialization
    void Awake () {
        animatorComponent = GetComponent<Animator>();
        edges = new List<EdgeSide>();

    }

    void Start ()
    {
        uid = FrameInfo.currentFrame.VertexUidCounter;
    }
	
	void OnMouseUp() {
        if (selectedVertex == null || selectedVertex == this)
        {
            SelectVertex();
        }
        else
        {
            ConnectToSelectedVertex();
        }

	}

    public void AddEdgeSide(EdgeSide edgeSide)
    {
        edges.Add(edgeSide);
    }

    public static void ConnectVertexes(Vertex first, Vertex second)
    {
        EdgeController line = Instantiate(GameInfo.edgePrefab).GetComponent<EdgeController>();
        line.transform.SetParent(FrameInfo.currentFrame.edges.transform);
        line.SetPosition(0, first);
        line.SetPosition(1, second);
        first.AddEdgeSide(new EdgeSide(line, 0));
        second.AddEdgeSide(new EdgeSide(line, 1));
    }

    private void ConnectToSelectedVertex()
    {
        ConnectVertexes(this, selectedVertex);
        selectedVertex.SelectVertex();
    }

    private void SelectVertex()
    {
        isSelected = !isSelected;
        animatorComponent.SetBool("isSelected", isSelected);
        if (isSelected)
        {
            selectedVertex = this;
        }
        else
        {
            selectedVertex = null;
        }
    }

    void OnMouseDrag()
    {
        Vector2 tmp = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        this.transform.position = tmp;

        for (int i = edges.Count - 1; i >= 0; --i)
        {
            if (!edges[i].trySetLocation(this))
            {
                edges.RemoveAt(i);
            }
        }
       
    }

    void OnDestroy()
    {
        if (edges == null)
        {
            return;
        }
        for (int i = edges.Count - 1; i >= 0; --i)
        {
            if (edges[i].Line != null)
            {
                Destroy(edges[i].Line.gameObject);
            }
        }
    }

    public static Vertex CreateVertex(Vector2 coordinates, FrameInfo frame = null)
    {
        if (frame == null)
        {
            frame = FrameInfo.currentFrame;
        }
        GameObject tmp = (GameObject)Instantiate(GameInfo.vertexPrefab, coordinates, new Quaternion());
        tmp.transform.parent = frame.vertexes.transform;
        return tmp.GetComponent<Vertex>();
    }
}
