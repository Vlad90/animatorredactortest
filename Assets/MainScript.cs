﻿using UnityEngine;
using System.Collections;
using System;

public class MainScript : MonoBehaviour {
    public GameObject framePrefab;
    public GameObject vertexPrefab;
    public GameObject edgePrefab;

    // Use this for initialization
    void Start () {
        GameInfo.framePrefab = framePrefab;
        GameInfo.vertexPrefab = vertexPrefab;
        GameInfo.edgePrefab = edgePrefab;
        GenerateFirstFrame();
    }

    private void GenerateFirstFrame()
    {
        GameObject tmp = (GameObject)Instantiate(framePrefab);
        FrameInfo.firstFrame = tmp.GetComponent<FrameInfo>();
    }

    // Update is called once per frame
    void Update () {
	
	}
}
