﻿using System;
using System.Collections.Generic;
using UnityEngine;

public class EdgeSide
{
    private EdgeController line;
    private int side;
    public EdgeSide(EdgeController line, int side)
    {
        this.line = line;
        this.side = side;
    }

    public EdgeController Line
    {
        get
        {
            return line;
        }
    }

    public bool trySetLocation(Vertex location)
    {
        if (line == null)
        {
            return false;
        }
        line.SetPosition(side, location);
        return true;
    }
}
