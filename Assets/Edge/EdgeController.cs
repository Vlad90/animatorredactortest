﻿using UnityEngine;
using System.Collections;

public class EdgeController : MonoBehaviour
{
    public static EdgeController selectedEdge;

    private LineRenderer lineRenderer;
    private BoxCollider2D lineCollider;
    private Animator animatorComponent;
    private Vector2[] positions;
    private Vertex[] vertexes;
    private bool isSelected = false;

    public Vertex[] Vertexes
    {
        get
        {
            return vertexes;
        }
    }

    void Awake()
    {
        lineRenderer = this.GetComponent<LineRenderer>();
        lineCollider = this.GetComponent<BoxCollider2D>();
        animatorComponent = GetComponent<Animator>();
        positions = new Vector2[2];
        vertexes = new Vertex[2];
    }

    public void SetPosition(int side, Vertex vertex)
    {
        if (side >= 2)
        {
            Debug.LogError("Line could not have more than two elements");
        }
        vertexes[side] = vertex;
        positions[side] = vertex.transform.position;
        lineRenderer.SetPosition(side, vertex.transform.position);
        UpdatePositions();
    }

    public void SelectEdge()
    {
        isSelected = !isSelected;
        animatorComponent.SetBool("isSelected", isSelected);
        if (isSelected)
        {
            selectedEdge = this;
        }
        else
        {
            selectedEdge = null;
        }
    }

    void OnMouseUp()
    {
        if (selectedEdge != null && selectedEdge != this)
        {
            selectedEdge.SelectEdge();
        }
        SelectEdge();

    }

    private void UpdatePositions()
    {
        Vector2 tmp = lineCollider.size;
        tmp.x = Mathf.Max(Vector2.Distance(positions[0], positions[1]) - 0.8f, 0);
        lineCollider.size = tmp;
        this.transform.position = (positions[0] + positions[1]) / 2;

        tmp = positions[0] - positions[1];
        float angle = Mathf.Atan2(tmp.y, tmp.x) * Mathf.Rad2Deg;
        transform.rotation = Quaternion.AngleAxis(angle, Vector3.forward);

    }
}
